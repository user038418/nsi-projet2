# ***NSI Projet 2***

# Objectifs
Création d'un programme de compatibilité amoureuse, sur la base de plusieurs critères.
-> Application de rencontre.


## Cahier des charges
- [x] Enregistrer des profils d'utilisateurs
- [ ] Comparer les utilisateur 


## Critères
Critère | Points
--- | ---
Sexe et orientation sexuelle | **éliminatoire** si pas compatibles
Age | **10** points
Ville | **15** points
Musique | **10** points
Cinéma | **10** points
Littérature | **10** points
Sports | **15** points
Couleur yeux | **5** points
Couleur cheveux | **5** points
Taille | **10** points
Imc | **5** points
Musculature | **5** points
**_Total_** | **100** points


## Tâches
Adrien | Colin
--- | ---
db | db
compatibilité | creation_bdd
classement | distance_villes
avis_utilisateur | creation_profils


## Prototypes UI
<img src="img/prototype_ui1.png" alt="Page de démarrage" title="Page de démarrage" width="500rem">
<img src="img/prototype_ui2.png" alt="Page des suggestions" title="Page des suggestions" width="500rem">


- - -
### __*Auteurs*__
Adrientertainment & Colindustries
