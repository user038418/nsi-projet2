# Importations
from distance_villes import coordonnees, dist_points, dist_villes
from math import isclose


def test_coordonnees():
    # donnees de test
    coord = '47.6597493766,-2.75714329498' # Vannes
    res_check = (47.6597493766, -2.75714329498)
    res_test = coordonnees(coord)
    
    #tests
    assert type(res_test) is tuple, "Le type n'est pas le bon"
    assert len(res_test) == 2, "La longueur n'est pas la bonne"
    assert type(res_test[0]) is float, "Le type de la première coordonnée n'est pas bon"
    assert type(res_test[1]) is float, "Le type de la deuxième coordonnée n'est pas bon"
    
    assert res_test[0] == res_check[0], "La première coordonnée n'est pas la bonne"
    assert res_test[1] == res_check[1], "La deuxième coordonnée n'est pas la bonne"
    
    print("coordonnees : ok")
    

def test_dist_points():
    # données de test
    lat1, lon1 = 47.2600100383, -2.09386404628 # Corsept
    lat2, lon2 = 47.6597493766, -2.75714329498 # Vannes
    distance_check = 66.7496247044924
    distance_test = dist_points(lat1, lon1, lat2, lon2)
    
    # tests
    assert type(distance_test) is float, "Le type n'est pas le bon"
    assert isclose(distance_test, distance_check), "La distance calculée n'est pas la bonne"
    
    print("dist_points : ok")


def test_dist_villes():
    # données de test
    ville1 = "RENNES"
    ville2 = "35690"
    distance_check = 13
    distance_test = dist_villes(ville1, ville2)
    
    # tests
    assert type(distance_test) is int, "Le type n'est pas le bon"
    assert distance_test == distance_check, "La distance calculée n'est pas la bonne"
    
    print("dist_villes : ok")
    

test_coordonnees()
test_dist_points()
test_dist_villes()