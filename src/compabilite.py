########################################
#     Fichier: compatibilite.py        #
########################################

# bibliothèque de calcul des distances
from distance_villes import *
from db import select_sql


def comp_age(age_ref, age_comp):
    """
          Fonction qui va comparer l'âge de deux personnes et qui va retourner une note sur 10
        
        Paramètre
        ---------
        age_ref: int
            âge de la personne de réference
            
        age_comp: int
            âge de la personne que l'on compare au réferant
        
        Sortie
        ------
        note: int
            note sur 10 qui varie suivant l'écart d'âge
    """
    assert type(age_ref) is int, "'age_ref' n'est pas un int"
    assert type(age_comp) is int, "'age_comp' n'est pas un int"

    # définition de la variable qui contient la différence d'âge
    if age_ref > age_comp:
        diff = age_ref - age_comp

    else:
        diff = age_comp - age_ref

    # définition de la variable note
    note = 10

    while diff > 0:

        if note <= 0:
            return 0

        note -= 1
        diff -= 2

    return note


def comp_ville(ville_ref, ville_comp):
    """
          Fonction qui va comparer la ville de deux personnes et qui va retourner une note sur 15
        
        Paramètre
        ---------
        ville_ref: str
            ville de la personne de réference
            
        ville_comp: str
            ville de la personne que l'on compare au réferant
            
        Sortie
        ------
        note: int
            note sur 15 qui varie selon l'écart des deux villes
        
    """
    assert type(ville_ref) is str, "'ville_ref' n'est pas un string"
    assert type(ville_comp) is str, "'ville_comp' n'est pas un string"

    # définition de la variable de distance
    dist = int(round(dist_villes(ville_ref, ville_comp), 0))

    # définition de la variable note
    note = 15

    while dist > 0:

        if note <= 0:
            return 0

        note -= 1
        dist -= 50

    return note


def comp_musique(musique_ref, musique_comp):
    """
          Fonction qui va comparer le goût pour la musique de deux personnes
        
        Paramètre
        ---------
        musique_ref: str
            goût pour la musique de la personne de réference
            
        musique_comp: str
            goût pour la musique de la personne que l'on compare au réferant
            
        Sortie
        ------
        note: int
            note sur 10 qui varie selon la différence de goût pour la musique des deux personnes
    """
    assert type(musique_ref) is str, "'musique_ref' n'est pas un string"
    assert type(musique_comp) is str, "'musique_comp' n'est pas un string"

    if musique_ref == musique_comp:
        return 10

    # définition des variables de goûts
    gouts = ('jamais', 'rarement', 'parfois', 'souvent')

    assert musique_ref in gouts, "'musique_ref' n'est pas un adverbe de 'gouts'"
    gout_ref = gouts.index(musique_ref) + 1

    assert musique_comp in gouts, "'musique_comp' n'est pas un adverbe de 'gouts'"
    gout_comp = gouts.index(musique_comp) + 1

    # définition de la variable de différence
    if gout_ref > gout_comp:
        diff = gout_ref - gout_comp

    else:
        diff = gout_comp - gout_ref

    # définition de la note
    return int(round(10 - diff / 4 * 10, 0))


def comp_cinema(cinema_ref, cinema_comp):
    """
          Fonction qui va comparer le goût pour le cinéma de deux personnes
          
        Paramètre
        ---------
        cinema_ref: str
            goût pour le cinéma de la personne de réference
            
        cinema_comp: str
            goût pour le cinéma de la personne que l'on compare au réferant
            
        Sortie
        ------
        note: int
            note sur 10 qui varie selon la différence de gout des deux personnes
    """
    assert type(cinema_ref) is str, "'cinema_ref' n'est pas un string"
    assert type(cinema_comp) is str, "'cinema_comp' n'est pas un string"

    if cinema_ref == cinema_comp:
        return 10

    # défintion des variables de goûts
    gouts = ('jamais', 'rarement', 'parfois', 'souvent')

    assert cinema_ref in gouts, "'cinema_ref' n'est pas un adverbe de 'gouts'"
    gout_ref = gouts.index(cinema_ref) + 1

    assert cinema_comp in gouts, "'cinema_comp' n'est pas un adverbe de 'gouts'"
    gout_comp = gouts.index(cinema_comp) + 1

    # définition de la variable de différence
    if gout_ref > gout_comp:
        diff = gout_ref - gout_comp

    else:
        diff = gout_comp - gout_ref

    # définition de la note
    return int(round(10 - diff / 4 * 10, 0))


def comp_litterature(litter_ref, litter_comp):
    """
          Fonction qui va comparer le goût pour la littérature de deux personnes
        
        Paramètre
        ---------
        litter_ref: str
            goût pour la littérature de la personne de réference
            
        litter_comp: str
            goût pour la littérature de la personne que l'on compare au réferant
            
        Sortie
        ------
        note: int
            note sur 10 qui varie selon la différence de goût pour la littérature des deux personnes
            
    """
    assert type(litter_ref) is str, "'litter_ref' n'est pas un string"
    assert type(litter_comp) is str, "'litter_comp' n'est pas un string"

    if litter_ref == litter_comp:
        return 10

    # définition des variables de goûts
    gouts = ('jamais', 'rarement', 'parfois', 'souvent')

    assert litter_ref in gouts, "'litter_ref' n'est pas un adverbe de 'gouts'"
    gout_ref = gouts.index(litter_ref) + 1

    assert litter_comp in gouts, "'litter_comp' n'est pas un adverbe de 'gouts'"
    gout_comp = gouts.index(litter_comp) + 1

    # définition de la variable de différence
    if gout_ref > gout_comp:
        diff = gout_ref - gout_comp

    else:
        diff = gout_comp - gout_ref

    # défintion de la note
    return int(round(10 - diff / 4 * 10, 0))


def comp_sport(sport_ref, sport_comp):
    """
          Fonction qui va comparer la pratique de sport de deux personnes
        
        Paramètre
        ---------
        sport_ref: str
            pratique de sport de la personne de réference
            
        sport_comp: str
            pratique de sport de la personne que l'on compare au réferant
            
        Sortie
        ------
        note: int
            note sur 15 qui varie selon la différence de pratique de sport des deux personnes
            
    """
    assert type(sport_ref) is str, "'sport_ref' n'est pas un string"
    assert type(sport_comp) is str, "'sport_comp' n'est pas un string"

    if sport_ref == sport_comp:
        return 15

    # définition des variables de goûts
    gouts = ('jamais', 'rarement', 'parfois', 'souvent')

    assert sport_ref in gouts, "'sport_ref' n'est pas un adverbe de 'gouts'"
    gout_ref = gouts.index(sport_ref) + 1

    assert sport_comp in gouts, "'sport_comp' n'est pas un adverbe de 'gouts'"
    gout_comp = gouts.index(sport_comp) + 1

    # définition de la variable de différence
    if gout_ref > gout_comp:
        diff = gout_ref - gout_comp

    else:
        diff = gout_comp - gout_ref

    # définition de la note
    return int(round(15 - diff / 3 * 15, 0))


def comp_yeux(yeux_ref, yeux_comp):
    """
          Fonction qui va comparer la couleurs des yeux de deux personnes
        
        Paramètre
        ---------
        yeux_ref: str
            couleur des yeux de la personne de réference
            
        yeux_comp: str
            couleur des yeux de la personne que l'on compare au réferant
            
        Sortie
        ------
        note: int
            note sur 5 qui varie selon la différence de couleur des yeux des deux personnes
        
    """
    assert type(yeux_ref) is str, "'yeux_ref' n'est pas un string"
    assert type(yeux_comp) is str, "'yeux_comp' n'est pas un string"

    if yeux_ref == yeux_comp:
        return 5

    # définition de la variable de couleurs
    couleur = ('noir', 'marron', 'vert', 'bleu', 'gris')

    assert yeux_ref in couleur, "'yeux_ref' n'est pas une couleur pris en charge"
    couleur_ref = couleur.index(yeux_ref) + 1

    assert yeux_comp in couleur, "'yeux_comp' n'est pas une couleur pris en charge"
    couleur_comp = couleur.index(yeux_comp) + 1

    # définition de la variable de différence
    if couleur_ref > couleur_comp:
        diff = couleur_ref - couleur_comp

    else:
        diff = couleur_comp - couleur_ref

    # définition de la note
    return int(round(5 - diff / 4 * 5, 0))


def comp_cheveux(cheveux_ref, cheveux_comp):
    """
          Fonction qui va comparer la couleur de cheveux de deux personnes
    
        Paramètre
        ---------
        cheveux_ref: str
            couleur de cheveux de la personne de réference
            
        cheveux_comp: str
            couleur de cheveux de la personne que l'on compare au réferant
            
        Sortie
        ------
        note: int
            note sur 5 qui varie selon la différence de couleur des deux personnes
        
    """
    assert type(cheveux_ref) is str, "'cheveux_ref' n'est pas un string"
    assert type(cheveux_comp) is str, "'cheveux_comp' n'est pas un string"

    if cheveux_ref == cheveux_comp:
        return 5

    # définition des variables de couleurs
    couleur = ('noir', 'brun', 'chatain', 'roux', 'gris', 'blond', 'blanc')

    assert cheveux_ref in couleur, "'cheveux_ref' n'est pas une couleur pris en charge"
    couleur_ref = couleur.index(cheveux_ref) + 1

    assert cheveux_comp in couleur, "'cheveux_comp' n'est pas une couleur pris en charge"
    couleur_comp = couleur.index(cheveux_comp) + 1

    # définition de la variable de différence
    if couleur_ref > couleur_comp:
        diff = couleur_ref - couleur_comp

    else:
        diff = couleur_comp - couleur_ref

    # définition de la note
    return int(round(5 - diff / 6 * 5, 0))


def comp_taille(taille_ref, taille_comp):
    """
          Fonction qui va comparer la taille de deux personnes
    
        Paramètre
        ---------
        taille_ref: int
            Taille de la personne de réference
            
        taille_comp: int
            Taille de la personne que l'on compare au réferant
            
        Sortie
        ------
        note: int
            note sur 10 qui varie selon la différence de couleur des deux personnes
        
    """
    assert type(taille_ref) is int, "'taille_ref' n'est pas un entier"
    assert taille_ref > 0, "' taille_ref' n'est pas supérieur à 0"
    assert type(taille_comp) is int, "'taille_comp' n'est pas un entier"
    assert taille_comp > 0, "' taille_comp' n'est pas supérieur à 0"

    # définition de la variable de différence
    if taille_ref == taille_comp:
        return 10

    if taille_ref > taille_comp:
        diff = taille_ref - taille_comp

    else:
        diff = taille_comp - taille_ref

    if diff >= 100:
        return 0

    return int(round(10 - diff / 10, 0))


def comp_imc(imc_ref, imc_comp):
    """
          Fonction qui va comparer l'Imc de deux personnes
    
        Paramètre
        ---------
        imc_ref: int
            Imc de la personne de réference
            
        imc_comp: int
            Imc de la personne que l'on compare au réferant
            
        Sortie
        ------
        note: int
            note sur 5 qui varie selon la différence de couleur des deux personnes
        
    """
    assert type(imc_ref) is int, "'imc_ref' n'est pas un entier"
    assert imc_ref > 0, "' imc_ref' n'est pas supérieur à 0"
    assert type(imc_comp) is int, "'imc_comp' n'est pas un entier"
    assert imc_comp > 0, "' imc_comp' n'est pas supérieur à 0"

    # définition de la variable de différence
    if imc_ref == imc_comp:
        return 5

    if imc_ref > imc_comp:
        diff = imc_ref - imc_comp

    else:
        diff = imc_comp - imc_ref

    diff = diff // 2

    if diff >= 5:
        return 0

    return int(round(5 - diff / 5, 0))


def comp_muscu(muscu_ref, muscu_comp):
    """
          Fonction qui va comparer la musculature de deux personnes
        
        Paramètre
        ---------
        muscu_ref: str
            Niveau de musculature de la personne de réferance
            
        muscu_comp: str
            Niveau de musculature de la personne que l'on compare au referant
            
        Sortie
        ------
        note: int
            Note sur 5 qui varie selon la différence de niveau de musculature des deux personnes comparées
    
    """
    assert type(muscu_ref) is str, "'muscu_ref' n'est pas un string"
    assert type(muscu_comp) is str, "'muscu_comp' n'est pas un string"

    # définition de la variable niveau
    niveau = ('faible', 'moyenne', 'forte')

    if muscu_ref == muscu_comp:
        return 5

    # définition des variables des niveaux des personnes comparées
    assert muscu_ref in niveau, "'muscu_ref' n'est pas un niveau de musculation reconnue"
    niv_ref = niveau.index(muscu_ref) + 1

    assert muscu_comp in niveau, "'muscu_comp' n'est pas un niveau de musculation reconnue"
    niv_comp = niveau.index(muscu_comp) + 1

    # définition de la variable de différence
    if niv_ref > niv_comp:
        diff = niv_ref - niv_comp

    else:
        diff = niv_comp - niv_ref

    # définition de la note
    if diff == 2:
        return 3

    else:
        return 0


def compatibilite(referant, compare):
    """
        Fonction qui va appeler toute les fonctions de compatibilité et retourner une note

        Paramètres
        ----------
        referant: dict
            dictionnaire contenant toute les informations sur le profil référant
        compare: dict
            dictionnaire contenant toute les informations sur le profil qui est comparé au profil référant

        Sortie
        ------
        note: int
            note sur 100 représentant la compatibilité du profil comparé par rapport au profil référant
    """
    assert type(referant) is dict, "'referant' n'est pas un dictionnaire"
    assert type(compare) is dict, "'compare' n'est pas un dictionnaire"

    # création de la variable note
    note = 0

    # compatibilité de l'âge
    note += comp_age(referant["age"], compare["age"])

    # compatibilité de la distance
    note += comp_ville(referant["commune"], compare["commune"])

    # compatibilité de la musique
    note += comp_musique(referant["freq_musique"], compare["freq_musique"])

    # compatibilité du cinéma
    note += comp_cinema(referant["freq_cinema"], compare["freq_cinema"])

    # compatibilité de la littérature
    note += comp_litterature(referant["freq_litterature"], compare["freq_litterature"])

    # compatibilité du sport
    note += comp_sport(referant["freq_sport"], compare["freq_sport"])

    # compatibilité des yeux
    note += comp_yeux(referant["coul_yeux"], compare["coul_yeux"])

    # compatibilité des cheveux
    note += comp_cheveux(referant["coul_cheveux"], compare["coul_cheveux"])

    # compatibilité de la taille
    note += comp_taille(referant["taille"], compare["taille"])

    # compatibilité de l'IMC
    note += comp_imc(referant["imc"], compare["imc"])

    # compatibilité de la musculature
    note += comp_muscu(referant["musculature"], compare["musculature"])

    assert note <= 100, "'note' n'est pas sur 100"
    return note


if __name__ == '__main__':
    infos = select_sql()
    print(infos)
    print(infos[0]['nom'], infos[1]['nom'], compatibilite(infos[0], infos[1]))
    print(infos[0]['nom'], infos[2]['nom'], compatibilite(infos[1], infos[2]))
