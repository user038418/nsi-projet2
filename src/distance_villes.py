from math import cos, radians, sqrt
from db import get_ville


def coordonnees(coord):
    """
        Fonction qui sépare extrait les coordonnées d'une chaine de caractères

        Paramètres 
        -----------
        coord : str
            les coordonnées à séparer et convertir en floattant
        
        Retours
        -------
        retourne : tuple de float
            les coordonnées extraites dans un tuple
    """
    tmp = coord.split(',')
    lat = float(tmp[0])
    lon = float(tmp[1])
    tmp.clear()
    return lat, lon


def dist_points(lat1, lon1, lat2, lon2):
    """
        Fonction qui calcule la distance entre deux points

        Paramètres 
        -----------
        lat1 : float
            latitude du premier point
            
        lon1 : float
            longitude du premier point
            
        lat2 : float
            latitude du deuxième point
            
        lon2 : float
            longitude du deuxième point
        
        Retours
        -------
        retourne : float
            la distance entre les deux points en km
    """
    lat_moy = radians((lat1 + lat2) / 2)
    x = (lon2 - lon1) * cos(lat_moy)
    y = lat2 - lat1
    z = sqrt(x ** 2 + y ** 2)  # Pythagore
    d = (40000 / 360) * z
    return d


def dist_villes(ville1, ville2, insee=False):
    """
        Fonction qui calcule la distance entre deux villes

        Paramètres 
        -----------
        ville1 : str
            Nom, code INSEE ou code postal de la première ville
            
        ville2 : str
            Nom, code INSEE ou code postal de la deuxième ville

        id : bool (facultatif)
            True si ville1 et ville2 sont des codes INSEE, False sinon
            Par défaut : False
            
        Retours
        -------
        retourne : float
            la distance entre les deux villes en km
    """
    if not insee:
        # Conversion en id
        ville1 = get_ville(ville1, "id_ville")['id_ville']
        ville2 = get_ville(ville2, "id_ville")['id_ville']

    # Récupération des coordonnées des villes
    donnees = [get_ville(ville1, "coord"), get_ville(ville2, "coord")]

    # Extraction des coordonnées
    c = coordonnees(donnees[0]['coord'])
    ville1 = (c[0], c[1], ville1)
    c = coordonnees(donnees[1]['coord'])
    ville2 = (c[0], c[1], ville2)

    # Calcul de la distance
    return round(dist_points(ville1[0], ville1[1], ville2[0], ville2[1]))


if __name__ == "__main__":
    v1, v2 = "35352", "35001"
    print(f"Distance entre {v1} et {v2} : {dist_villes(v1, v2, True)} km")
