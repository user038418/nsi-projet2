# Importations
from db import select_sql, TOUTES_COLONES
from distance_villes import dist_villes
from creation_profil import ajout_profil, recup_infos
from compabilite import compatibilite


def afficher_profil(ref, comp, pronom):
    """
        Affiche les informations relatives à un profil donné

        Paramètres
        ----------
        ref : dict
            Informations de l'utilisateur actif
        
        comp : dict
            Informations du profil à afficher

        pronom : str
            Pronom à utiliser en fonction du sexe du profil à afficher

        Retours
        -------
        aucun
    """
    print(f"{comp['prenom']} {comp['nom']} a {comp['age']} ans.", end=" ")
    print(f"{pronom} habite à {comp['commune']}, à {dist_villes(comp['id_ville'], ref['id_ville'], True)} km "
          f"de chez vous.", end="\n\n")

    print(
        f"{pronom} va {comp['freq_cinema']} au cinéma, lis {comp['freq_litterature']} des livres,"
        f"écoute {comp['freq_musique']} de la musique et fait {comp['freq_sport']} du sport.",
        end="\n\n")

    print(f"{pronom} a les yeux {comp['coul_yeux']} et les cheveux {comp['coul_cheveux']}. ")
    print(f"{pronom} mesure {comp['taille']} cm pour {comp['poids']} kg et a une musculature {comp['musculature']}.")


def register():
    """
        Demande à l'utilisateur s'il veut s'inscrire puis l'enregistre ou non

        Paramètres
        ----------
        aucun

        Retours
        -------
        retourne : dict
            Informations enregistrées sur l'utilisateur
    """
    res = input("Créer un compte maintenant ? oui/non ")
    while res not in ("o", "oui", "n", "non"):
        print("Merci de ne répondre que par oui ou non.")
        res = input("Créer un compte maintenant ? oui/non ")

    if res in ("o", "oui"):
        infos = recup_infos()
        return ajout_profil(infos)
    else:
        print("A bientôt !")
        quit()


def login(count=0):
    """
        Retrouve les informations d'un utilisateur à partir de l'adresse mail et offre 5 tentatives

        Paramètres
        ----------
        count : int (facultatif)
            Nombre d'itérations de la fonction

        Retours
        -------
        retourne : dict
            Informations enregistrées sur l'utilisateur
    """
    # Sortir de la boucle après 5 tentatives
    if count > 4:
        print("C'est votre 5ème échec... Etes-vous sûr d'avoir un compte avec nous ?")
        print("Vous pouvez en créer un maintenant si vous le souhaitez !")
        register()

    # Récupération adresse mail
    mail = input("Quelle est votre adresse mail ? ")
    while "@" not in mail and "." not in mail:
        print("\nMerci d'entrer une adresse mail valide.")
        mail = input("Quel est votre adresse mail ? ")

    # Tentative récupération ID utilisateur
    try:
        util = select_sql(TOUTES_COLONES, f"WHERE mail = '{mail}' LIMIT 1")[0]
        return util

    # Retour au début de login() si échec
    except IndexError:
        print("Nous ne parvenons pas à trouver votre compte...")
        print("Merci de réessayer\n")
        return login(count + 1)


def app():
    """
        Exécute l'application et assemble tous les modules

        Paramètres
        ----------
        aucun

        Retours
        -------
        aucun
    """
    print("Bonjour!")
    print("Bienvenue sur le programme de rencontre d'Adrientertainment et Colindustries :)")

    existant = input("Avez-vous déjà un compte avec nous ? oui/non ")
    while existant not in ("o", "oui", "n", "non"):
        print("\nMerci de ne répondre que par oui ou non.")
        existant = input("Avez-vous déjà un compte avec nous ? oui/non ")

    print()

    if existant in ("o", "oui"):
        infos_util = login()

        print(f"\nRe-bonjour {infos_util['prenom']} !")

        aff_profils = input("Voulez-vous regarder des profils de personnes autour de vous ? oui/non ")
        while aff_profils not in ("o", "oui", "n", "non"):
            print("Merci de ne répondre que par oui ou non.")
            aff_profils = input("Voulez-vous regarder des profils de personnes autour de vous ? oui/non ")

        if aff_profils in ("oui", "o"):
            profils = select_sql(TOUTES_COLONES, f"WHERE id_util != {infos_util['id_util']}")
            print(profils)
            profils.sort(reverse=False, key=lambda x: compatibilite(infos_util, x))
            print(profils)

            for i in range(len(profils)):
                print(f"\n\nProfil {i + 1} :")

                if profils[i]['sexe'] == 'f':
                    afficher_profil(infos_util, profils[i], "Elle")
                else:
                    afficher_profil(infos_util, profils[i], "Il")

        else:
            print("A bientôt !")
            quit()

    else:
        register()


app()
