from db import insert_sql, select_sql, get_ville


def ville_to_id(ville):
    """
        Convertit un nom de ville ou un code postal en code INSEE

        Paramètres 
        -----------
        ville : str ou int
            Nom ou code postal de la ville à convertir
        
        Retours
        -------
        retourne : int
            Code INSEE de la ville
    """
    if type(ville) is str:
        ville = ville.upper()
        if ville == "PARIS":
            return 75101

    try:
        ville = get_ville(ville, "id_ville")['id_ville']
        return int(ville)

    except IndexError:
        print("Nous ne parvenons pas à trouver votre ville...")
        ville = input("Entrez le nom ou le code postal de votre ville : ")
        return ville_to_id(ville)



def ajout_profil(donnees):
    """
        Fonction qui récupère les informations à insérer dans le profil

        Paramètres 
        -----------
        donnees : dict de str ou dict
            Contient toutes les données relatives à l'utilisateur à enregistrer
        
        Retours
        -------
        retourne : bool
            True si l'opération a fonctionné
    """
    # Vérifications des formats des données
    test_nom = type(donnees['nom']) is str
    test_prenom = type(donnees['prenom']) is str
    test_age = type(donnees['age']) is int
    test_sexe = donnees['sexe'] in ("h", "f", "non-binaire")
    test_orientation = donnees['orientation'] in ("h", "f", "h&f", "non-binaire", "tous")

    test_ville = type(donnees['ville']) is int
    if test_ville:
        donnees['ville'] = ville_to_id(donnees['ville'])

    test_freq_musique = donnees['hobbies']['freq_musique'] in ('jamais', 'rarement', 'parfois', 'souvent')
    test_freq_cinema = donnees['hobbies']['freq_cinema'] in ('jamais', 'rarement', 'parfois', 'souvent')
    test_freq_litterature = donnees['hobbies']['freq_litterature'] in ('jamais', 'rarement', 'parfois', 'souvent')
    test_freq_sport = donnees['hobbies']['freq_sport'] in ('jamais', 'rarement', 'parfois', 'souvent')
    test_hobbies = test_freq_musique and test_freq_cinema and test_freq_litterature and test_freq_sport

    test_coul_yeux = donnees['traits_physiques']['coul_yeux'] in ('marron', 'vert', 'bleu', 'noir', 'gris')
    test_coul_cheveux = donnees['traits_physiques']['coul_cheveux'] in ('brun', 'chatain', 'roux', 'noir', 'gris', 'blond', 'blanc')
    test_taille = type(donnees['traits_physiques']['taille']) is int
    test_poids = type(donnees['traits_physiques']['poids']) is int
    test_imc = type(donnees['traits_physiques']['imc']) is int
    test_musculature = donnees['traits_physiques']['musculature'] in ('faible', 'moyenne', 'forte')
    test_traits_physiques = test_coul_yeux and test_coul_cheveux and test_taille and test_poids and test_imc and test_musculature

    if test_nom and test_prenom and test_age and test_sexe and test_orientation and test_hobbies and test_traits_physiques:
        res = []

        # Désignation de l'ID utilisateur
        prec = select_sql("id_util", "ORDER BY id_util DESC LIMIT 1")
        print("ID précédent : ", prec[0]['id_util'])
        id_util = prec[0]['id_util'] + 1

        # Hobbies
        clés = "id_hobbies, freq_musique, freq_cinema, freq_litterature, freq_sport"
        valeurs = f"{id_util}, '{donnees['hobbies']['freq_musique']}', '{donnees['hobbies']['freq_cinema']}', '{donnees['hobbies']['freq_litterature']}', '{donnees['hobbies']['freq_sport']}'"
        res.append(insert_sql("Hobbies", clés, valeurs))

        # Physique
        clés = "id_phys, coul_yeux, coul_cheveux, taille, poids, imc, musculature"
        valeurs = f"{id_util}, '{donnees['traits_physiques']['coul_yeux']}', '{donnees['traits_physiques']['coul_cheveux']}', {donnees['traits_physiques']['taille']}, {donnees['traits_physiques']['poids']}, {donnees['traits_physiques']['imc']}, '{donnees['traits_physiques']['musculature']}'"
        res.append(insert_sql("Physique", clés, valeurs))

        # Profil complet
        clés = "id_util, nom, prenom, age, mail, sexe, orientation, ville"
        valeurs = f"{id_util}, '{donnees['nom']}', '{donnees['prenom']}', '{donnees['age']}', '{donnees['mail']}', '{donnees['sexe']}', '{donnees['orientation']}', {donnees['ville']}"
        res.append(insert_sql("Profils", clés, valeurs))

        if False in res:
            print("Erreur.")
            return False
        else:
            print("ok.")
            return id_util

    else:
        print('Erreur test.')
        return False


def recup_infos():
    """
        Fonction qui récupère les informations à insérer dans le profil

        Paramètres 
        -----------
        aucun
        
        Retours
        -------
        retourne : dict de str ou dict
            les informations récupérées
    """
    # Récupération nom et prénom
    nom = input("Quel est votre nom ? ")
    prenom = input("Quel est votre prenom ? ")

    # Récupération age obligatoirement sous forme de chiffres
    age = input("Quel âge avez-vous ? ")
    while not age.isdigit():
        print("\nMerci de ne répondre qu'avec un nombre entier.")
        age = input("Quel âge avez-vous ? ")
    age = int(age)
    assert age >= 18, "Vous êtes trop jeune, merci de revenir dans quelques années"

    # Récupération adresse mail
    mail = input("Quelle est votre adresse mail ? ")
    while "@" not in mail and "." not in mail:
        print("\nMerci d'entrer une adresse mail valide.")
        mail = input("Quel est votre adresse mail ? ")

    # Récupération sexe sous forme h/f/non-binaire
    sexe = input("Quel est votre sexe ? (h/f/non-binaire) ")
    while sexe not in ("h", "f", "non-binaire"):
        print("\nMerci de ne répondre que par h, f ou non-binaire.")
        sexe = input("Quel est votre sexe ? (h/f/non-binaire) ")

    # Récupération orientation sexuelle sous forme homosexuel/heterosexuel/bisexuel/bi
    orientation = input("Vous êtes attirés par les : h/f/h&f/non-binaire/tous ")
    while orientation not in ("h", "f", "h&f", "non-binaire", "tous"):
        print("\nMerci de ne répondre que par h, f, h&f, non-binaire ou tous.")
        orientation = input("Vous êtes attirés par les : h/f/h&f/non-binaire/tous ")

    # Récupération ville sous forme de code postal
    ville = input("Quel est votre code postal ? ")
    while not ville.isdigit():
        print("\nMerci de ne répondre que par des chiffres.")
        ville = input("Quel est votre code postal ? ")
    ville = int(ville)

    # Récupération hobbies
    hobbies = {}
    # Musique
    hobbies['freq_musique'] = input(
        "A quelle fréquence écoutez-vous de la musique ? (jamais/rarement/parfois/souvent) ")
    while hobbies['freq_musique'] not in ('jamais', 'rarement', 'parfois', 'souvent'):
        print("\nMerci de ne répondre que par jamais, rarement, parfois ou souvent.")
        hobbies['freq_musique'] = input(
            "A quelle fréquence écoutez-vous de la musique ? (jamais/rarement/parfois/souvent) ")

        # Cinéma
    hobbies['freq_cinema'] = input(
        "A quelle fréquence allez-vous au cinéma ou regardez-vous des films/séries ? (jamais/rarement/parfois/souvent) ")
    while hobbies['freq_cinema'] not in ('jamais', 'rarement', 'parfois', 'souvent'):
        print("\nMerci de ne répondre que par jamais, rarement, parfois ou souvent.")
        hobbies['freq_cinema'] = input(
            "A quelle fréquence allez-vous au cinéma ou regardez-vous des films/séries ? (jamais/rarement/parfois/souvent) ")

        # Littérature
    hobbies['freq_litterature'] = input("A quelle fréquence lisez-vous des livres ? (jamais/rarement/parfois/souvent) ")
    while hobbies['freq_litterature'] not in ('jamais', 'rarement', 'parfois', 'souvent'):
        print("\nMerci de ne répondre que par jamais, rarement, parfois ou souvent.")
        hobbies['freq_litterature'] = input(
            "A quelle fréquence lisez-vous des livres ? (jamais/rarement/parfois/souvent) ")

        # Sport
    hobbies['freq_sport'] = input("A quelle fréquence faites-vous du sport ? (jamais/rarement/parfois/souvent) ")
    while hobbies['freq_sport'] not in ('jamais', 'rarement', 'parfois', 'souvent'):
        print("\nMerci de ne répondre que par jamais, rarement, parfois ou souvent.")
        hobbies['freq_sport'] = input("A quelle fréquence faites-vous du sport ? (jamais/rarement/parfois/souvent) ")

    # Récupération traits physiques
    traits_physiques = {}
    # Couleur des yeux
    traits_physiques['coul_yeux'] = input("Quelle est la couleur de vos yeux ? (marron/vert/bleu/noir/gris) ")
    while traits_physiques['coul_yeux'] not in ('marron', 'vert', 'bleu', 'noir', 'gris'):
        print("\nMerci de ne répondre que par marron, vert, bleu, noir ou gris.")
        traits_physiques['coul_yeux'] = input("Quelle est la couleur de vos yeux ? (marron/vert/bleu/noir/gris) ")

        # Couleur des yeux
    traits_physiques['coul_cheveux'] = input(
        "Quelle est la couleur de vos cheveux ? (brun/chatain/roux/noir/gris/blond/blanc) ")
    while traits_physiques['coul_cheveux'] not in ('brun', 'chatain', 'roux', 'noir', 'gris', 'blond', 'blanc'):
        print("\nMerci de ne répondre que par brun, chatain, roux, noir, gris, blond ou blanc.")
        traits_physiques['coul_cheveux'] = input(
            "Quelle est la couleur de vos cheveux ? (brun/chatain/roux/noir/gris/blond/blanc) ")

        # Taille
    traits_physiques['taille'] = input("Quelle est votre taille en cm ? ")
    while not traits_physiques['taille'].isdigit():
        print("\nMerci de ne répondre qu'avec des chiffres.")
        traits_physiques['taille'] = input("Quelle est votre taille en cm ? ")
    traits_physiques['taille'] = int(traits_physiques['taille'])

    # Poids
    traits_physiques['poids'] = input("Quelle est votre poids en kg ? ")
    while not traits_physiques['poids'].isdigit():
        print("\nMerci de ne répondre qu'avec des chiffres.")
        traits_physiques['poids'] = input("Quelle est votre poids en kg ? ")
    traits_physiques['poids'] = int(traits_physiques['poids'])

    # IMC
    traits_physiques['imc'] = int(traits_physiques['poids'] // ((traits_physiques['taille'] / 100) ** 2))

    # Musculature
    traits_physiques['musculature'] = input("Quelle est votre musculature ? (faible/moyenne/forte) ")
    while traits_physiques['musculature'] not in ('faible', 'moyenne', 'forte'):
        print("\nMerci de ne répondre que par faible, moyenne ou forte")
        traits_physiques['musculature'] = input("Quelle est votre musculature ? (faible/moyenne/forte) ")

    return {'nom': nom, 'prenom': prenom, 'age': age, 'mail': mail, 'sexe': sexe, 'orientation': orientation,
            'ville': ville, 'hobbies': hobbies, 'traits_physiques': traits_physiques}


if __name__ == "__main__":
    # infos = recup_infos()
    infos = {'nom': 'Arwell', 'prenom': 'Lily', 'age': 18, 'mail': 'lily@arwell.com', 'sexe': 'f',
             'orientation': 'tous', 'ville': 61000,
             'hobbies': {'freq_musique': 'jamais', 'freq_cinema': 'rarement', 'freq_litterature': 'parfois',
                         'freq_sport': 'souvent'},
             'traits_physiques': {'coul_yeux': 'gris', 'coul_cheveux': 'roux', 'taille': 215, 'poids': 132, 'imc': 28,
                                  'musculature': 'forte'}}
    print(f"\n{infos}\n")
    print(ajout_profil(infos))
