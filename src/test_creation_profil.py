from creation_profil import recup_infos, ville_to_id


def test_recup_infos():
    # donnees de test
    infos_test = recup_infos()

    # tests
    assert type(infos_test) is dict, "Le type n'est pas le bon"
    assert len(infos_test) == 9, "La longueur n'est pas la bonne"
    assert type(infos_test['nom']) is str, "Le type du nom n'est pas bon"
    assert type(infos_test['prenom']) is str, "Le type du prénom n'est pas bon"
    assert type(infos_test['age']) is int, "Le type du prénom n'est pas bon"
    assert type(infos_test['mail']) is str, "Le type du prénom n'est pas bon"
    assert type(infos_test['sexe']) is str, "Le type du prénom n'est pas bon"
    assert type(infos_test['orientation']) is str, "Le type du prénom n'est pas bon"
    assert type(infos_test['ville']) is int, "Le type du prénom n'est pas bon"
    assert type(infos_test['hobbies']) is dict, "Le type du prénom n'est pas bon"
    assert type(infos_test['traits_physiques']) is dict, "Le type du prénom n'est pas bon"

    assert len(infos_test['hobbies']) == 4, "Le type de hobbies n'est pas bon"
    assert type(infos_test['hobbies']['freq_musique']) is str, "Le type de la fréquence musique n'est pas bon"
    assert type(infos_test['hobbies']['freq_cinema']) is str, "Le type de la fréquence cinema n'est pas bon"
    assert type(infos_test['hobbies']['freq_litterature']) is str, "Le type de la fréquence litterature n'est pas bon"
    assert type(infos_test['hobbies']['freq_sport']) is str, "Le type de la fréquence sport n'est pas bon"

    assert len(infos_test['traits_physiques']) == 6, "Le type de traits_physiques n'est pas bon"
    assert type(infos_test['traits_physiques']['coul_yeux']) is str, "Le type de la couleur des yeux n'est pas bon"
    assert type(infos_test['traits_physiques']['coul_cheveux']) is str, "Le type de la couleur des cheveux n'est pas bon"
    assert type(infos_test['traits_physiques']['taille']) is int, "Le type de la taille n'est pas bon"
    assert type(infos_test['traits_physiques']['poids']) is int, "Le type du poids n'est pas bon"
    assert type(infos_test['traits_physiques']['imc']) is int, "Le type de l'imc n'est pas bon"
    assert type(infos_test['traits_physiques']['musculature']) is str, "Le type de la musculature n'est pas bon"

    print("récupération infos : ok")


def test_ville_to_id():
    #  test
    ville_check1 = 56260
    ville_check2 = 35001
    ville_test1 = ville_to_id("Vannes")
    ville_test2 = ville_to_id(35690)

    # tests
    assert type(ville_test1) is int, "Le type retourné avec un nom de ville n'est pas le bon"
    assert type(ville_test2) is int, "Le type retourné avec un code postal n'est pas le bon"
    assert ville_test1 == ville_check1, f"L'id retourné avec le nom de ville ({ville_test1}) n'est pas le bon"
    assert ville_test2 == ville_check2, f"L'id retourné  avec la code postal ({ville_test2}) n'est pas le bon"

    print("ville to id : ok")


if __name__ == '__main__':
    test_ville_to_id()
    test_recup_infos()
